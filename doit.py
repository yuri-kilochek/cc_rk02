# coding: UTF-8

from pprint import pprint
from collections import deque

def fmtset(syms):
    return ' '.join('$' if s is None else
                    'ε' if s == '' else
                     s for s in syms) or '∅'

def fmtlist(syms):
    syms = ' '.join('$'if s is None else
                     s for s in syms).strip()
    return syms.strip() or 'ε'

grammar = [
    ('S', ('print', '(', 'E', ')', ';',)),
    ('S', ('while', '(', 'B', ')', 'S',)),
    ('S', ('{', 'L', '}',)),
    ('E', ('id',)),
    ('E', ('num',)),
    ('B', ('E', '>', 'E',)),
    ('L', ('S', 'L',)),
    ('L', ()),
]
print('Грамматика:')
for i, (lhs, rhs) in enumerate(grammar):
    print('  ({}) '.format(i), lhs, '->', fmtlist(rhs))
print()

variables = set()
for lhs, rhs in grammar:
    variables |= {lhs}

terminals = set()
for lhs, rhs in grammar:
    for symbol in rhs:
        if symbol not in variables:
            terminals |= {symbol}


first1 = {'': {''}}
for terminal in terminals:
    first1[terminal] = {terminal}
for variable in variables:
    first1[variable] = {'' for lhs, rhs in grammar
                           if lhs == variable and rhs == ()}
def first1_(alpha):
    result = set()
    for symbol in alpha:
        result |= first1[symbol] - {''}
        if '' not in first1[symbol]:
            break
    if all('' in first1[s] for s in alpha):
        result |= {''}
    return result
while True:
    old_first1 = {s: fs.copy() for s, fs in first1.items()}
    for lhs, rhs in grammar:
        first1[lhs] |= first1_(rhs)
    if first1 == old_first1:
        break
print('Множества FIRST₁ sets:')
for v in variables:
    print('  FIRST₁({}) = {}'.format(v, fmtset(first1[v])))
print()


follow1 = {v: ({None} if v == grammar[0][0] else set()) for v in variables}
while True:
    old_follow1 = {s: fs.copy() for s, fs in follow1.items()}
    for lhs, rhs in grammar:
        for i, symbol in enumerate(rhs):
            if symbol not in variables:
                continue
            follow1[symbol] |= first1_(rhs[i + 1:]) - {''}
            if '' in first1_(rhs[i + 1:]):
                follow1[symbol] |= follow1[lhs]
    if follow1 == old_follow1:
        break
print('Множества FOLLOW₁:')
for v in variables:
    print('  FOLLOW₁({}) = {}'.format(v, fmtset(follow1[v])))
print()


print('Проверим, является ли грамматика LL(1):')
is_ll1 = True
for i in range(len(grammar)):
    lhs1, rhs1 = grammar[i]
    for j in range(i + 1, len(grammar)):
        lhs2, rhs2 = grammar[j]
        if lhs1 == lhs2 and rhs1 != rhs2:
            a = {x for l1 in follow1[lhs1] for x in first1_((*rhs1, l1))}
            b = {y for l2 in follow1[lhs2] for y in first1_((*rhs2, l2))}
            s = a & b
            print('  FIRST₁({} FOLLOW₁({})) ∩ FIRST₁({} FOLLOW₁({})) = {} ∩ {} = {}'.format(fmtlist(rhs1), lhs1, fmtlist(rhs2), lhs2, fmtset(a), fmtset(b), fmtset(s)))
            is_ll1 &= not s
if is_ll1:
    print('=> Грамматика является LL(1)')
else:
    print('=> Грамматика не является LL(1)')
    exit()
print()

transitions = {}
for i, (lhs, rhs) in enumerate(grammar):
    for a in first1_(rhs) - {''}:
        transitions[lhs, a] = i
    if '' in first1_(rhs):
        for b in follow1[lhs]:
            transitions[lhs, b] = i
print('Управляющая таблица:')
print('    ', end='')
for t in terminals:
    print(' {} '.format(t), end='')
print()
print()
for v in variables:
    print(' {:>2} '.format(v), end='')
    for t in terminals:
        print((' {:^' + str(len(t)) + '} ').format(transitions.get((v, t), '')), end='')
    print()
print()

input = (
    '{',
        'while', '(', 'id', '>', 'num', ')',
            'print', '(', 'id', ')', ';',
        'print', '(', 'num', ')', ';',
    '}',
)
print('Входная цепочка:')
print(' ', fmtlist(input))
print()

stack = ('S', '$',)
output = ()
trace = []
while True:
    trace.append((fmtlist(input), fmtlist(stack), fmtlist(map(str, output))))
    if stack[0] in variables and len(input) > 0 and (stack[0], input[0]) in transitions:
        i = transitions[stack[0], input[0]]
        stack = (*grammar[i][1], *stack[1:])
        output = (*output, i)
    elif len(input) > 0 and input[0] == stack[0]:
        input = input[1:]
        stack = stack[1:]
    else:
        break
input_width = 0
stack_width = 0
output_width = 0
for input, stack, output in trace:
    input_width = max(input_width, len(input))
    stack_width = max(stack_width, len(stack))
    output_width = max(output_width, len(output))
print('Разбор:')
for i, (input, stack, output) in enumerate(trace):
    print('  (   {}   ,   {}   ,   {}   )'.format(
        ('{:>' + str(input_width) + '}').format(input),
        ('{:>' + str(stack_width) + '}').format(stack),
        ('{:<' + str(output_width) + '}').format(output),
    ), end='')
    if i < len(trace) - 1:
        print(' ⊢')
    else:
        print()




